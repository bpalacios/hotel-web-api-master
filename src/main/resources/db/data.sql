INSERT INTO categoria_cuarto (nombre,descripcion,precio)
    VALUES ('Individual', 'Ideal para quienes viajan solos.',50.0);
INSERT INTO categoria_cuarto (nombre,descripcion,precio)
    VALUES ('De Lujo', 'Ideal para los exigentes',150.0);    
INSERT INTO categoria_cuarto (nombre,descripcion,precio)
    VALUES ('Economicos', 'Ideal para grupos familiares.',250.0);
INSERT INTO categoria_cuarto (nombre,descripcion,precio)
    VALUES ('Cabañas', 'Cabañas',150.0);
    

INSERT INTO cuarto (numero, descripcion,categoria)
    VALUES(1,'Vista a la piscina',1);
INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(2,'Remodelado recientemente',1);
INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(3,'Suit Presidencial',2);
INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(4,'Cuarto Económico',2);
INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(5,'Cuarto Secreto',3);
INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(6,'Cuarto sin ventanas',3);
 INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(7,'Cuarto para grupos familiares',3);
 INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(8,'Cuarto para descansar',3);
  INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(9,'Cuarto para grupos',3);
  INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(10,'Cuarto Japones',3);
 INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(11,'Cuarto de Hamacas',3);
  INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(12,'Cuarto de Tablas',3);
  
  
  INSERT INTO huesped(nombre, email, telefono)
  values('Bladimir Palacios L.', 'blad77@hotmail.com', '1111-2222');
  
  INSERT INTO reservacion(desde, hasta, cuarto, huesped)
  values('2017-02-12', '2017-02-13', 1, 1);
  
  
  INSERT INTO recibo(total, reservacion)
  values(150, 1);
  