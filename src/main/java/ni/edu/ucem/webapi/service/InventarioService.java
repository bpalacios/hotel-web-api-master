package ni.edu.ucem.webapi.service;


import ni.edu.ucem.webapi.modelo.CategoriaCuarto;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.modelo.Reservacion;
import ni.edu.ucem.webapi.modelo.Recibo;
import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.modelo.Filtro;

public interface InventarioService 
{
	public void agregarCategoriaCuarto(final CategoriaCuarto pCategoriaCuarto);
    public void guardarCategoriaCuarto(final CategoriaCuarto pCategoriaCuarto);
    public void eliminarCategoriaCuarto(final int id);
    public CategoriaCuarto obtenerCategoriaCuarto(final int id);
    public Pagina<CategoriaCuarto> obtenerTodosCategoriaCuarto(final Filtro paginacion);

    
    public void agregarCuarto(final Cuarto pCuarto);
    public void guardarCuarto(final Cuarto pCuarto);
    public void eliminarCuarto(final int pCuarto);
    public Cuarto obtenerCuarto(final int pCuarto);
    public Pagina<Cuarto> obtenerTodosCuarto(final Filtro paginacion);
    public Pagina<Cuarto> obtenerTodosCuartoEnCategoria(final int pCategoriaCuarto, final Filtro paginacion);
   
    public void agregarHuesped(final Huesped pHuesped);
    public void guardarHuesped(final Huesped pHuesped);
    public void eliminarHuesped(final int id);  
    public Huesped obtenerHuesped(final int id);    
    public Pagina<Huesped> obtenerTodosHuesped(final Filtro paginacion);
    
    public void agregarReservacion(final Reservacion pReservacion);
    public void guardarReservacion(final Reservacion pReservacion);
    public void eliminarReservacion(final int id);   
    public Reservacion obtenerReservacion(final int id);    
    public Pagina<Reservacion> obtenerTodosReservacion(final Filtro paginacion);
        
   
    public void agregarRecibo(final Recibo pRecibo);
    public void guardarRecibo(final Recibo pRecibo);
    public void eliminarRecibo(final int id);   
    public Recibo obtenerRecibo(final int id);    
    public Pagina<Recibo> obtenerTodosRecibo(final Filtro paginacion);
    
    
}
