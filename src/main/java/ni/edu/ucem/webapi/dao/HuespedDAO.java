package ni.edu.ucem.webapi.dao;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import ni.edu.ucem.webapi.modelo.Huesped;

public interface HuespedDAO 
{
	
	@PreAuthorize("hasRole('ADMIN')")
	public Huesped obtenerPorId(final int pId);
    
    public int contar();

    public List<Huesped> obtenerTodos(final int offset, final int limit);

    public void agregar(final Huesped pHuesped);

    public void guardar(final Huesped pHuesped);

    public void eliminar(final int pId);
	

}
