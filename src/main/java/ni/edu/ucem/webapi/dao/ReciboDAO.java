package ni.edu.ucem.webapi.dao;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;

import ni.edu.ucem.webapi.modelo.Recibo;

public interface ReciboDAO {

	@PreAuthorize("hasRole('ADMIN')")
	public Recibo obtenerPorId(final int pId);
    
    public int contar();

    public List<Recibo> obtenerTodos(final int offset, final int limit);

    public void agregar(final Recibo pRecibo);

    public void guardar(final Recibo pRecibo);

    public void eliminar(final int pId);	
	
}
