package ni.edu.ucem.webapi.dao;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import ni.edu.ucem.webapi.modelo.Reservacion;

public interface ReservacionDAO {

	@PreAuthorize("hasRole('ADMIN')")
	public Reservacion obtenerPorId(final int pId);
    
    public int contar();

    public List<Reservacion> obtenerTodos(final int offset, final int limit);

    public void agregar(final Reservacion pReservacion);

    public void guardar(final Reservacion pReservacion);

    public void eliminar(final int pId);
	
}
