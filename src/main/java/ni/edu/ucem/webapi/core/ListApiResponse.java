package ni.edu.ucem.webapi.core;

import ni.edu.ucem.webapi.core.ApiResponse.Status;
import ni.edu.ucem.webapi.modelo.Pagina;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ni.edu.ucem.webapi.core.ApiResponse.ApiError;

@JsonInclude(Include.NON_NULL)
public class ListApiResponse<T>
{
    private final Status status;
    private final Pagina<T> data;
    private final ApiError error;
    

    public ListApiResponse(final Status status, final Pagina<T> data) 
    {
        this(status, data, null);
    }
    
    public ListApiResponse(final Status status, Pagina<T> data, final ApiError error) 
    {
        this.status = status;
        this.data = data;
        this.error = error;
    }
    
    public Status getStatus() {
        return status;
    }

    public Pagina<T> getData() {
        return data;
    }

    public ApiError getError() {
        return error;
    }
}
