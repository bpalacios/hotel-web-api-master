package ni.edu.ucem.webapi.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ni.edu.ucem.webapi.modelo.CategoriaCuarto;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.modelo.Reservacion;
import ni.edu.ucem.webapi.modelo.Recibo;

import ni.edu.ucem.webapi.dao.CategoriaCuartoDAO;
import ni.edu.ucem.webapi.dao.CuartoDAO;
import ni.edu.ucem.webapi.dao.HuespedDAO;
import ni.edu.ucem.webapi.dao.ReservacionDAO;
import ni.edu.ucem.webapi.dao.ReciboDAO;

import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.modelo.Filtro;
import ni.edu.ucem.webapi.service.InventarioService;

@Service
public class InventarioServiceImpl implements InventarioService 
{
    private final CategoriaCuartoDAO categoriaCuartoDAO;
    private final CuartoDAO cuartoDAO;
    private final HuespedDAO huespedDAO;   
    private final ReservacionDAO reservacionDAO;
    private final ReciboDAO reciboDAO;
    
    public InventarioServiceImpl(
    		final CategoriaCuartoDAO categoriaCuartoDAO,
    		final CuartoDAO cuartoDAO,
    		final HuespedDAO huespedDAO,
    		final ReservacionDAO reservacionDAO,
    		final ReciboDAO reciboDAO
    		
    		)
    {
        this.categoriaCuartoDAO = categoriaCuartoDAO;
        this.cuartoDAO = cuartoDAO;
        this.huespedDAO = huespedDAO;
        this.reservacionDAO = reservacionDAO;
        this.reciboDAO = reciboDAO;
    }
    
    @Transactional
    @Override
    public void agregarCategoriaCuarto(final CategoriaCuarto pCategoriaCuarto) 
    {
        this.categoriaCuartoDAO.agregar(pCategoriaCuarto);
    }

    @Transactional
    @Override
    public void guardarCategoriaCuarto(final CategoriaCuarto pCategoriaCuarto) 
    {
        if(pCategoriaCuarto.getId() < 1)
        {
            throw new IllegalArgumentException("La categoría del cuarto no existe");
        }
        this.categoriaCuartoDAO.guardar(pCategoriaCuarto);
    }

    @Transactional
    @Override
    public void eliminarCategoriaCuarto(final int pId) 
    {
        if(pId < 1)
        {
            throw new IllegalArgumentException("ID invalido. Debe ser mayor a cero");
        }
        this.categoriaCuartoDAO.eliminar(pId);
    }

    @Override
    public CategoriaCuarto obtenerCategoriaCuarto(final int pId) 
    {
        return this.categoriaCuartoDAO.obtenerPorId(pId);
    }

    @Override
    public Pagina<CategoriaCuarto> obtenerTodosCategoriaCuarto(Filtro paginacion) 
    {
        //return this.categoriaCuartoDAO.obtenerTodos();
        
        List<CategoriaCuarto> categoriacuartos;
        final int count = this.categoriaCuartoDAO.contar();
        if(count > 0)
        {
        	categoriacuartos = this.categoriaCuartoDAO.obtenerTodos(paginacion.getOffset(),
                    paginacion.getLimit());
        }
        else
        {
        	categoriacuartos= new ArrayList<CategoriaCuarto>();
        }
        return new Pagina<CategoriaCuarto>(categoriacuartos, count,  paginacion.getOffset(), paginacion.getLimit());
        
        
    }
    
    @Transactional
    @Override
    public void agregarCuarto(final Cuarto pCuarto) 
    {
        this.cuartoDAO.agregar(pCuarto);

    }
    
    @Transactional
    @Override
    public void guardarCuarto(final Cuarto pCuarto) 
    {
        if(pCuarto.getId() < 1)
        {
            throw new IllegalArgumentException("El cuarto no existe");
        }
        this.cuartoDAO.guardar(pCuarto);
    }
    
    @Transactional
    @Override
    public void eliminarCuarto(final int pId) 
    {
        if(pId < 1)
        {
            throw new IllegalArgumentException("ID invalido. Debe ser mayor a cero");
        }
        this.cuartoDAO.eliminar(pId);
    }

    @Override
    public Cuarto obtenerCuarto(final int pId) 
    {
        if (pId < 0) 
        {
            throw new IllegalArgumentException("ID inválido. debe ser mayor a cero.");
        }
        return this.cuartoDAO.obtenerPorId(pId); 
    }

    @Override
    public Pagina<Cuarto> obtenerTodosCuarto(Filtro paginacion) 
    {
        List<Cuarto> cuartos;
        final int count = this.cuartoDAO.contar();
        if(count > 0)
        {
            cuartos = this.cuartoDAO.obtenerTodos(paginacion.getOffset(),
                    paginacion.getLimit());
        }
        else
        {
            cuartos = new ArrayList<Cuarto>();
        }
        return new Pagina<Cuarto>(cuartos, count,  paginacion.getOffset(), paginacion.getLimit());
    }

    @Override
    public Pagina<Cuarto> obtenerTodosCuartoEnCategoria(final int pCategoriaCuartoId, final Filtro paginacion)
    {
        final int count = this.cuartoDAO.contarPorCategoria(pCategoriaCuartoId);
        List<Cuarto> cuartos = null;
        if(count > 0)
        {
            cuartos = this.cuartoDAO.obtenerTodosPorCategoriaId(pCategoriaCuartoId, paginacion.getOffset(),
                    paginacion.getLimit());
        }
        else
        {
            cuartos = new ArrayList<Cuarto>();
        }
        return new Pagina<Cuarto>(cuartos, count,  paginacion.getOffset(), paginacion.getLimit());
    }
    
    
    
    @Transactional
    @Override
    public void agregarHuesped(final Huesped pHuesped) 
    {
        this.huespedDAO.agregar(pHuesped);
    }

    
    @Transactional
    @Override
    public void guardarHuesped(final Huesped pHuesped) 
    {
        if(pHuesped.getId() < 1)
        {
            throw new IllegalArgumentException("El huesped no existe");
        }
        this.huespedDAO.guardar(pHuesped);
    }
    

    @Transactional
    @Override
    public void eliminarHuesped(final int pId) 
    {
        if(pId < 1)
        {
            throw new IllegalArgumentException("ID invalido. Debe ser mayor a cero");
        }
        this.huespedDAO.eliminar(pId);
    }

    
    @Override
    public Huesped obtenerHuesped(final int pId) 
    {
        return this.huespedDAO.obtenerPorId(pId);
    }
    

    @Override
    public Pagina<Huesped> obtenerTodosHuesped(Filtro paginacion) 
    {
       
        
        List<Huesped> huesped;
        final int count = this.huespedDAO.contar();
        if(count > 0)
        {
        	huesped = this.huespedDAO.obtenerTodos(paginacion.getOffset(),
                    paginacion.getLimit());
        }
        else
        {
        	huesped= new ArrayList<Huesped>();
        }
        return new Pagina<Huesped>(huesped, count,  paginacion.getOffset(), paginacion.getLimit());
        
        
    }
    
    
    
    @Transactional
    @Override
    public void agregarReservacion(final Reservacion pReservacion) 
    {
        this.reservacionDAO.agregar(pReservacion);
    }

    
    @Transactional
    @Override
    public void guardarReservacion(final Reservacion pReservacion) 
    {
        if(pReservacion.getId() < 1)
        {
            throw new IllegalArgumentException("El id Reservacion no existe");
        }
        this.reservacionDAO.guardar(pReservacion);
    }
    

    @Transactional
    @Override
    public void eliminarReservacion(final int pId) 
    {
        if(pId < 1)
        {
            throw new IllegalArgumentException("ID invalido. Debe ser mayor a cero");
        }
        this.reservacionDAO.eliminar(pId);
    }

    
    @Override
    public Reservacion obtenerReservacion(final int pId) 
    {
        return this.reservacionDAO.obtenerPorId(pId);
    }
    

    @Override
    public Pagina<Reservacion> obtenerTodosReservacion(Filtro paginacion) 
    {
        
        List<Reservacion> Reservacion;
        final int count = this.reservacionDAO.contar();
        if(count > 0)
        {
        	Reservacion = this.reservacionDAO.obtenerTodos(paginacion.getOffset(),
                    paginacion.getLimit());
        }
        else
        {
        	Reservacion= new ArrayList<Reservacion>();
        }
        return new Pagina<Reservacion>(Reservacion, count,  paginacion.getOffset(), paginacion.getLimit());
        
        
    }
    
    
    
    @Transactional
    @Override
    public void agregarRecibo(final Recibo pRecibo) 
    {
        this.reciboDAO.agregar(pRecibo);
    }

    
        
    @Transactional
    @Override
    public void guardarRecibo(final Recibo pRecibo) 
    {
        if(pRecibo.getId() < 1)
        {
            throw new IllegalArgumentException("El recibo no existe");
        }
        this.reciboDAO.guardar(pRecibo);
    }
    

    @Transactional
    @Override
    public void eliminarRecibo(final int pId) 
    {
        if(pId < 1)
        {
            throw new IllegalArgumentException("ID invalido. Debe ser mayor a cero");
        }
        this.reciboDAO.eliminar(pId);
    }

    
    @Override
    public Recibo obtenerRecibo(final int pId) 
    {
        return this.reciboDAO.obtenerPorId(pId);
    }
    

    @Override
    public Pagina<Recibo> obtenerTodosRecibo(Filtro paginacion) 
    {
        
        List<Recibo> Recibo;
        final int count = this.reciboDAO.contar();
        if(count > 0)
        {
        	Recibo = this.reciboDAO.obtenerTodos(paginacion.getOffset(),
                    paginacion.getLimit());
        }
        else
        {
        	Recibo = new ArrayList<Recibo>();
        }
        return new Pagina<Recibo>(Recibo, count,  paginacion.getOffset(), paginacion.getLimit());
        
        
    }
    
    
    
}
