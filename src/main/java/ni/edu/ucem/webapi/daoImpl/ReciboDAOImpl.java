package ni.edu.ucem.webapi.daoImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import ni.edu.ucem.webapi.dao.ReciboDAO;
import ni.edu.ucem.webapi.modelo.Recibo;

@Repository
public class ReciboDAOImpl implements ReciboDAO {

private final JdbcTemplate jdbcTemplate;
	
	@Autowired
    public ReciboDAOImpl(final JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Recibo obtenerPorId(final int pId) 
    {
        final String sql = "select * from recibo where id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{pId}, 
                new BeanPropertyRowMapper<Recibo>(Recibo.class));
    }

    
    @Override
    public int contar()
    {
        final String sql = "select count(*) from recibo";
        return this.jdbcTemplate.queryForObject(sql, Integer.class);
    }
    
    
    @Override
    public List<Recibo> obtenerTodos(final int offset, final int limit) 
    {
        final String sql = "select * from recibo";
        return this.jdbcTemplate.query(sql,
                new BeanPropertyRowMapper<Recibo>(Recibo.class));
    }

    @Override
    public void agregar(final Recibo pRecibo) 
    {
        final String sql = new StringBuilder()
                .append("INSERT INTO recibo")
                .append(" ")
                .append("(total, reservacion)")
                .append(" ")
                .append("VALUES(?,?)")
                .toString();
        final Object[] parametros = new Object[2];
        parametros[0] = pRecibo.getTotal();
        parametros[1] = pRecibo.getReservacion();
        this.jdbcTemplate.update(sql,parametros);
    }

    @Override
    public void guardar(final Recibo pRecibo) 
    {
        final String sql = new StringBuilder()
                .append("UPDATE recibo")
                .append(" ")
                .append("SET total = ?, reservacion = ?")
                .append(" ")
                .append("WHERE id = ?")
                .toString();
        final Object[] parametros = new Object[5];
        parametros[0] = pRecibo.getTotal();
        parametros[1] = pRecibo.getReservacion();
        parametros[2] = pRecibo.getId();
        this.jdbcTemplate.update(sql,parametros);
    }

    @Override
    public void eliminar(final int pId) 
    {
        final String sql = "delete from recibo where id = ?";
        this.jdbcTemplate.update(sql, new Object[]{pId});
    }
	
}
