package ni.edu.ucem.webapi.daoImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import ni.edu.ucem.webapi.dao.ReservacionDAO;
import ni.edu.ucem.webapi.modelo.Reservacion;

@Repository
public class ReservacionDAOImpl implements ReservacionDAO{
	
	private final JdbcTemplate jdbcTemplate;
	
	@Autowired
    public ReservacionDAOImpl(final JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Reservacion obtenerPorId(final int pId) 
    {
        final String sql = "select * from reservacion where id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{pId}, 
                new BeanPropertyRowMapper<Reservacion>(Reservacion.class));
    }

    
    @Override
    public int contar()
    {
        final String sql = "select count(*) from reservacion";
        return this.jdbcTemplate.queryForObject(sql, Integer.class);
    }
    
    
    @Override
    public List<Reservacion> obtenerTodos(final int offset, final int limit) 
    {
        final String sql = "select * from reservacion";
        return this.jdbcTemplate.query(sql,
                new BeanPropertyRowMapper<Reservacion>(Reservacion.class));
    }

    @Override
    public void agregar(final Reservacion pReservacion) 
    {
        final String sql = new StringBuilder()
                .append("INSERT INTO reservacion")
                .append(" ")
                .append("(desde, hasta, cuarto, huesped)")
                .append(" ")
                .append("VALUES(?,?,?,?)")
                .toString();
        final Object[] parametros = new Object[4];
        parametros[0] = pReservacion.getDesde();
        parametros[1] = pReservacion.getHasta();
        parametros[2] = pReservacion.getCuarto();
        parametros[3] = pReservacion.getHuesped();
        this.jdbcTemplate.update(sql,parametros);
    }

    @Override
    public void guardar(final Reservacion pReservacion) 
    {
        final String sql = new StringBuilder()
                .append("UPDATE reservacion")
                .append(" ")
                .append("SET desde = ?, hasta = ?")
                .append(",cuarto = ?, huesped = ?")
                .append(" ")
                .append("WHERE id = ?")
                .toString();
        final Object[] parametros = new Object[5];
        parametros[0] = pReservacion.getDesde();
        parametros[1] = pReservacion.getHasta();
        parametros[2] = pReservacion.getCuarto();
        parametros[3] = pReservacion.getHuesped();
        parametros[4] = pReservacion.getId();
        this.jdbcTemplate.update(sql,parametros);
    }

    @Override
    public void eliminar(final int pId) 
    {
        final String sql = "delete from huesped where id = ?";
        this.jdbcTemplate.update(sql, new Object[]{pId});
    }
	
	

}
