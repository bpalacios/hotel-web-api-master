package ni.edu.ucem.webapi.daoImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import ni.edu.ucem.webapi.dao.HuespedDAO;
import ni.edu.ucem.webapi.modelo.Huesped;

@Repository
public class HuespedDAOImpl implements HuespedDAO{
	
	private final JdbcTemplate jdbcTemplate;
	
	@Autowired
    public HuespedDAOImpl(final JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Huesped obtenerPorId(final int pId) 
    {
        final String sql = "select * from huesped where id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{pId}, 
                new BeanPropertyRowMapper<Huesped>(Huesped.class));
    }

    
    @Override
    public int contar()
    {
        final String sql = "select count(*) from huesped";
        return this.jdbcTemplate.queryForObject(sql, Integer.class);
    }
    
    
    @Override
    public List<Huesped> obtenerTodos(final int pOffset, final int pLimit) 
    {
        final String sql = "select * from huesped offset ? limit ?";
        return this.jdbcTemplate.query(sql, new Object[]{pOffset, pLimit},
                new BeanPropertyRowMapper<Huesped>(Huesped.class));
    }

    @Override
    public void agregar(final Huesped pHuesped) 
    {
        final String sql = new StringBuilder()
                .append("INSERT INTO huesped")
                .append(" ")
                .append("(nombre, email, telefono)")
                .append(" ")
                .append("VALUES(?,?,?)")
                .toString();
        final Object[] parametros = new Object[3];
        parametros[0] = pHuesped.getNombre();
        parametros[1] = pHuesped.getEmail();
        parametros[2] = pHuesped.getTelefono();
        this.jdbcTemplate.update(sql,parametros);
    }

    @Override
    public void guardar(final Huesped pHuesped) 
    {
        final String sql = new StringBuilder()
                .append("UPDATE huesped")
                .append(" ")
                .append("SET nombre = ?")
                .append(",email = ?")
                .append(",telefono = ?")
                .append(" ")
                .append("WHERE id = ?")
                .toString();
        final Object[] parametros = new Object[4];
        parametros[0] = pHuesped.getNombre();
        parametros[1] = pHuesped.getEmail();
        parametros[2] = pHuesped.getTelefono();
        parametros[3] = pHuesped.getId();
        this.jdbcTemplate.update(sql,parametros);
    }

    @Override
    public void eliminar(final int pId) 
    {
        final String sql = "delete from huesped where id = ?";
        this.jdbcTemplate.update(sql, new Object[]{pId});
    }
	
	

}
