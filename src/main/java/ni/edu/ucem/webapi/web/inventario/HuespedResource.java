package ni.edu.ucem.webapi.web.inventario;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ni.edu.ucem.webapi.core.ApiResponse;
import ni.edu.ucem.webapi.core.ApiResponse.Status;
import ni.edu.ucem.webapi.core.ListApiResponse;
import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.modelo.Filtro;
import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.serviceImpl.InventarioServiceImpl;
import ni.edu.ucem.webapi.service.InventarioService;

@RestController
@RequestMapping("/v1/inventario/huespedes")
public class HuespedResource 
{
    private final InventarioService inventarioService;
    
    @Autowired
    public HuespedResource(final InventarioServiceImpl inventarioService)
    {
        this.inventarioService = inventarioService;
    }
    
    @RequestMapping(method = RequestMethod.GET,produces="application/json")
    public ListApiResponse<Huesped> obtenerHuesped( @RequestParam(value = "offset", required = false, defaultValue ="0") final Integer offset,
            @RequestParam(value = "limit", required = false, defaultValue="0") final Integer limit)
    {
        final Filtro paginacion = new Filtro.Builder()
				.paginacion(offset, limit)
				.build();
        Pagina<Huesped> pagina;
        pagina = this.inventarioService.obtenerTodosHuesped(paginacion);
        return new ListApiResponse<Huesped>(Status.OK, pagina);
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces="application/json")
    public ApiResponse obtener(@PathVariable("id") final int id)
    {
            final Huesped huesped = this.inventarioService.obtenerHuesped(id);
            return new ApiResponse(Status.OK, huesped);        
    }
    
    
    
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ApiResponse guardarHuesped(@Valid @RequestBody final Huesped huesped, BindingResult result)
    {
    	if(result.hasErrors())
        {
            throw new IllegalArgumentException(result.getFieldError().getDefaultMessage());
        }
        this.inventarioService.agregarHuesped(huesped);
        return new ApiResponse(Status.OK, huesped);
    }
    
    
    /**
     * Negociación de contenido. Aceptamos form-parameters para la creación de un nuevo recurso.
     * @param nombre
     * @param descripcion
     * @param precio
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ApiResponse guardarHuespedConFormData(final String nombre, final String email, final String telefono) 
    {
        Huesped huesped = new Huesped(nombre, email, telefono);
        this.inventarioService.agregarHuesped(huesped);
        return new ApiResponse(Status.OK, huesped);
    }
    
    
    
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT,
            produces="application/json")
    public ApiResponse guardarHuesped(@PathVariable("id") final int id, 
            @RequestBody final Huesped huespedActualizado) 
    {
        final Huesped huesped = new Huesped(id,
                huespedActualizado.getNombre(),
                huespedActualizado.getEmail(),
                huespedActualizado.getTelefono());
        this.inventarioService.guardarHuesped(huesped);
        return new ApiResponse(Status.OK, huesped);
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, 
            produces="application/json")
    public ApiResponse eliminarHuesped(@PathVariable("id") final int id) 
    {
            final Huesped huesped = this.inventarioService.obtenerHuesped(id);
            this.inventarioService.eliminarHuesped(huesped.getId());
            return new ApiResponse(Status.OK,null);      
    }
}
