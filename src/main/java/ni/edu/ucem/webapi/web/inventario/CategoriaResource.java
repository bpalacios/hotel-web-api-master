package ni.edu.ucem.webapi.web.inventario;

import java.math.BigDecimal;
//import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ni.edu.ucem.webapi.core.ApiResponse;
import ni.edu.ucem.webapi.core.ApiResponse.Status;
import ni.edu.ucem.webapi.core.ListApiResponse;
import ni.edu.ucem.webapi.modelo.CategoriaCuarto;
import ni.edu.ucem.webapi.modelo.Filtro;
import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.serviceImpl.InventarioServiceImpl;
import ni.edu.ucem.webapi.service.InventarioService;

@RestController
@RequestMapping("/v1/inventario/categorias")
public class CategoriaResource 
{
    private final InventarioService inventarioService;
    
    @Autowired
    public CategoriaResource(final InventarioServiceImpl inventarioService)
    {
        this.inventarioService = inventarioService;
    }
    
    @RequestMapping(method = RequestMethod.GET,produces="application/json")
    public ListApiResponse<CategoriaCuarto> obtenerCategorias(            @RequestParam(value = "offset", required = false, defaultValue ="0") final Integer offset,
            @RequestParam(value = "limit", required = false, defaultValue="0") final Integer limit)
    {
        final Filtro paginacion = new Filtro.Builder()
				.paginacion(offset, limit)
				.build();
        Pagina<CategoriaCuarto> pagina;
        pagina = this.inventarioService.obtenerTodosCategoriaCuarto(paginacion);
        return new ListApiResponse<CategoriaCuarto>(Status.OK, pagina);
    }
    
    
    
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces="application/json")
    public ApiResponse obtener(@PathVariable("id") final int id)
    {
            final CategoriaCuarto categoria = this.inventarioService.obtenerCategoriaCuarto(id);
            return new ApiResponse(Status.OK, categoria);        
    }
    
    
    
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ApiResponse guardarCategoriaCuarto(@Valid @RequestBody final CategoriaCuarto categoria, BindingResult result)
    {
    	if(result.hasErrors())
        {
            throw new IllegalArgumentException(result.getFieldError().getDefaultMessage());
        }
        this.inventarioService.agregarCategoriaCuarto(categoria);
        return new ApiResponse(Status.OK, categoria);
    }
    
    
    /**
     * Negociación de contenido. Aceptamos form-parameters para la creación de un nuevo recurso.
     * @param nombre
     * @param descripcion
     * @param precio
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ApiResponse guardarCategoriaCuartoConFormData(final String nombre, final String descripcion, final BigDecimal precio) 
    {
        CategoriaCuarto categoria = new CategoriaCuarto(nombre, descripcion, precio);
        this.inventarioService.agregarCategoriaCuarto(categoria);
        return new ApiResponse(Status.OK, categoria);
    }
    
    
       
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT,
            produces="application/json")
    public ApiResponse guardarCategoriaCuarto(@PathVariable("id") final int id, 
            @RequestBody final CategoriaCuarto categoriaActualizado) 
    {
        final CategoriaCuarto categoria = new CategoriaCuarto(id,
                categoriaActualizado.getDescripcion(),
                categoriaActualizado.getNombre(),
                categoriaActualizado.getPrecio());
        this.inventarioService.guardarCategoriaCuarto(categoria);
        return new ApiResponse(Status.OK, categoria);
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, 
            produces="application/json")
    public ApiResponse eliminarCategoriaCuarto(@PathVariable("id") final int id) 
    {
            final CategoriaCuarto categoria = this.inventarioService.obtenerCategoriaCuarto(id);
            this.inventarioService.eliminarCategoriaCuarto(categoria.getId());
            return new ApiResponse(Status.OK,null);      
    }
}
