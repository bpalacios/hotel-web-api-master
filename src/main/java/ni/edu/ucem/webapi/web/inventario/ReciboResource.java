package ni.edu.ucem.webapi.web.inventario;


import java.math.BigDecimal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ni.edu.ucem.webapi.core.ApiResponse;
import ni.edu.ucem.webapi.core.ApiResponse.Status;
import ni.edu.ucem.webapi.core.ListApiResponse;
import ni.edu.ucem.webapi.modelo.Recibo;
import ni.edu.ucem.webapi.modelo.Filtro;
import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.serviceImpl.InventarioServiceImpl;
import ni.edu.ucem.webapi.service.InventarioService;

@RestController
@RequestMapping("/v1/inventario/recibos")
public class ReciboResource 
{
    private final InventarioService inventarioService;
    
    @Autowired
    public ReciboResource(final InventarioServiceImpl inventarioService)
    {
        this.inventarioService = inventarioService;
    }
    
    @RequestMapping(method = RequestMethod.GET,produces="application/json")
    public ListApiResponse<Recibo> obtenerReservacion( @RequestParam(value = "offset", required = false, defaultValue ="0") final Integer offset,
            @RequestParam(value = "limit", required = false, defaultValue="0") final Integer limit)
    {
        final Filtro paginacion = new Filtro.Builder()
				.paginacion(offset, limit)
				.build();
        Pagina<Recibo> pagina;
        pagina = this.inventarioService.obtenerTodosRecibo(paginacion);
        return new ListApiResponse<Recibo>(Status.OK, pagina);
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces="application/json")
    public ApiResponse obtener(@PathVariable("id") final int id)
    {
            final Recibo recibo = this.inventarioService.obtenerRecibo(id);
            return new ApiResponse(Status.OK, recibo);        
    }
    
    
    
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ApiResponse guardarHuesped(@Valid @RequestBody final Recibo recibo, BindingResult result)
    {
    	if(result.hasErrors())
        {
            throw new IllegalArgumentException(result.getFieldError().getDefaultMessage());
        }
        this.inventarioService.agregarRecibo(recibo);
        return new ApiResponse(Status.OK, recibo);
    }
    
    
    /**
     * Negociación de contenido. Aceptamos form-parameters para la creación de un nuevo recurso.
     * @param nombre
     * @param descripcion
     * @param precio
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ApiResponse guardarReciboConFormData(final BigDecimal total, final Integer reservacion) 
    {
    	Recibo recibo = new Recibo(total, reservacion);
        this.inventarioService.agregarRecibo(recibo);
        return new ApiResponse(Status.OK, recibo);
    }
    
    
    
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT,
            produces="application/json")
    public ApiResponse guardarRecibo(@PathVariable("id") final int id, 
            @RequestBody final Recibo reciboActualizado) 
    {
        final Recibo recibo = new Recibo( id,
        		reciboActualizado.getTotal(),
        		reciboActualizado.getReservacion()
        		);
        this.inventarioService.guardarRecibo(recibo);
        return new ApiResponse(Status.OK, recibo);
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, 
            produces="application/json")
    public ApiResponse eliminarRecibo(@PathVariable("id") final int id) 
    {
            final Recibo recibo = this.inventarioService.obtenerRecibo(id);
            this.inventarioService.eliminarRecibo(recibo.getId());
            return new ApiResponse(Status.OK,null);      
    }
}
