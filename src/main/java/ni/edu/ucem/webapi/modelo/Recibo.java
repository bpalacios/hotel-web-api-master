package ni.edu.ucem.webapi.modelo;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Range;

public class Recibo {

	private Integer id;
	
	@Pattern(regexp="^[-+]?\\d+(\\.\\d+)?$")
	private BigDecimal total;
	
	@NotNull (message = "El número de reservacion es requerido")
    @Range(min=1,max=Short.MAX_VALUE)
	private Integer reservacion;


	public Recibo( ) {
		
	}
	
	
	public Recibo( final BigDecimal total, final Integer reservacion) {
		this.total = total;
		this.reservacion = reservacion;
	}
	
	public Recibo(final Integer id, final BigDecimal total, final Integer reservacion) {
		this.id = id;
		this.total = total;
		this.reservacion = reservacion;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Integer getReservacion() {
		return reservacion;
	}

	public void setReservacion(Integer reservacion) {
		this.reservacion = reservacion;
	}
	
	
	
	
}
