package ni.edu.ucem.webapi.modelo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;


public class Huesped {
	
	private Integer id;
	
	/**
     * ^Anchor the regex at the start of the string
       [\w ] Match an alphanumeric character, underscore or space
       + one or more times
       $ Anchor the regex at the end of the string
     */
    @NotNull
    @NotEmpty(message = "El nombre del huesped es requerido.")
    @Pattern(regexp = "^[\\w ]+$")
	private String nombre;
    
       
	@NotNull
    @NotEmpty(message = "El correo es requerido.") 
    @Email
    private String email;

    @NotNull
    @NotEmpty(message = "El telefono es requerido.")
    @Pattern(regexp = "\\d{4}-\\d{4}")
    private String telefono;

    public Huesped(){
    	
    }
	
    public Huesped(final String nombre, final String email, final String telefono) {
		this.nombre = nombre;
		this.email = email;
		this.telefono = telefono;
	}
    
    public Huesped(final Integer id, final String nombre, final String email, final String telefono) {
		this.id = id;
		this.nombre = nombre;
		this.email = email;
		this.telefono = telefono;
	}

    
    
    
    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
    

}
